## Commande Pizza Mario

Mario, le propriétaire d'une pizzeria à Nancy souhaite mettre à disposition de ses clients des bornes tactiles leur permettant de passer commande en autonomie.

```mermaid
flowchart TB

a((Client))
b((Mario))
A(Commande)
A'(Interface)
B[Commander une pizza existante]
C[(Liste de recette)]
D[Retirer ingrédients]
E[Composer sa pizza]
F[(Liste des ingrédients)]
G[(Tarifs)]
H[Passer la commande]

style a  fill:#F99
style b  fill:#f99
style A  fill:#ffc
style A'  fill:#ffc
style H  fill:#9f9

a==>A
A==>E
A==>B
B==>H
E<-.->F
E==>H
B<-.->C
B-->D
D-->B
D<-.->C
A'-.->G
b-->A'
A'-.->C
A'-.->F
```

![Cas](Cahier des Charges/User_Case.png)

