-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 31 oct. 2021 à 15:53
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bd_pizza`
--

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_command` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id`, `time_command`) VALUES
(108, '2021-10-31 15:53:07'),
(109, '2021-10-31 15:53:08'),
(110, '2021-10-31 15:53:08');

-- --------------------------------------------------------

--
-- Structure de la table `ingredient`
--

DROP TABLE IF EXISTS `ingredient`;
CREATE TABLE IF NOT EXISTS `ingredient` (
  `id` int(120) NOT NULL AUTO_INCREMENT,
  `nom` varchar(60) NOT NULL,
  `quantite` int(11) NOT NULL DEFAULT 10,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ingredient`
--

INSERT INTO `ingredient` (`id`, `nom`, `quantite`) VALUES
(2, 'base crême', 10),
(3, 'base tomate', 10),
(4, 'jambon', 10),
(5, 'olive', 10),
(6, 'emmental', 10),
(7, 'mozarella', 10),
(8, 'pepperoni', 10),
(9, 'poivron', 10),
(10, 'ananas', 10),
(11, 'champignon', 10),
(12, 'basilic', 10),
(13, 'oignon', 10);

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

DROP TABLE IF EXISTS `panier`;
CREATE TABLE IF NOT EXISTS `panier` (
  `id_commande` int(11) NOT NULL,
  `id_pizza` int(11) NOT NULL,
  `ingredient` varchar(600) NOT NULL,
  PRIMARY KEY (`id_commande`,`id_pizza`),
  KEY `id_pizza` (`id_pizza`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `panier`
--

INSERT INTO `panier` (`id_commande`, `id_pizza`, `ingredient`) VALUES
(108, 1, '\n -base tomate\n -mozarella\n -basilic\n\n'),
(108, 2, '\n -base tomate\n -jambon\n -mozarella\n -champignon\n\n'),
(109, 3, '\n -base tomate\n -jambon\n -mozarella\n -champignon\n\n'),
(110, 0, '\n -base crême\n -jambon\n -olive\n -mozarella\n\n');

-- --------------------------------------------------------

--
-- Structure de la table `pizza`
--

DROP TABLE IF EXISTS `pizza`;
CREATE TABLE IF NOT EXISTS `pizza` (
  `id` int(120) NOT NULL AUTO_INCREMENT,
  `nom` varchar(60) NOT NULL,
  `prix` int(10) NOT NULL DEFAULT 10,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pizza`
--

INSERT INTO `pizza` (`id`, `nom`, `prix`) VALUES
(0, 'custom', 8),
(1, 'margherita', 10),
(2, 'reine', 10),
(3, 'hawaienne', 12);

-- --------------------------------------------------------

--
-- Structure de la table `recette`
--

DROP TABLE IF EXISTS `recette`;
CREATE TABLE IF NOT EXISTS `recette` (
  `id_pizza` int(120) NOT NULL,
  `id_ingredient` int(120) NOT NULL,
  PRIMARY KEY (`id_pizza`,`id_ingredient`),
  KEY `id_ingredient` (`id_ingredient`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `recette`
--

INSERT INTO `recette` (`id_pizza`, `id_ingredient`) VALUES
(1, 3),
(1, 7),
(1, 12),
(2, 3),
(2, 4),
(2, 7),
(2, 11),
(3, 3),
(3, 4),
(3, 7),
(3, 10);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `panier`
--
ALTER TABLE `panier`
  ADD CONSTRAINT `panier_ibfk_2` FOREIGN KEY (`id_commande`) REFERENCES `commande` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `recette`
--
ALTER TABLE `recette`
  ADD CONSTRAINT `recette_ibfk_1` FOREIGN KEY (`id_pizza`) REFERENCES `pizza` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `recette_ibfk_2` FOREIGN KEY (`id_ingredient`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
