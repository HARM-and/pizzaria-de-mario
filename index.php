<?php

include_once("modele/AbstractClassLoader.php");
include_once("modele/ClassLoader.php");

$loader = new modele\ClassLoader(".");
$loader->register();

$piz = new modele\Pizza;
$ing = new modele\Ingredient;




//Début du script commencant à l'arrivée d'un clientsur la borne :

echo "\nLE CLIENT A ARRIVE SUR LA BORNE\n\n";
$current_commande = new modele\Commande;

//Affichage de la carte
$piz->getCarte();

//Selection d'une pizza et affichage de sa recette
$current_pizza = $piz->getPizzaById(1);
echo "Vous avez sélectionné une pizza : ".strtoupper($current_pizza[0]["nom"])." - ".$current_pizza[0]["prix"]."€";
$current_ingredient = $ing->getIngredientByPizzaId($current_pizza[0]["id"]);
echo $current_ingredient;

//Ajout de la pizza au panier
$current_commande->addPanier($current_pizza,$current_ingredient);

//Selection d'une pizza et affichage de sa recette
$current_pizza = $piz->getPizzaById(2);
echo "Vous avez sélectionné une pizza : ".strtoupper($current_pizza[0]["nom"])." - ".$current_pizza[0]["prix"]."€";
$current_ingredient = $ing->getIngredientByPizzaId($current_pizza[0]["id"]);
echo $current_ingredient;

//Ajout de la pizza au panier
$current_commande->addPanier($current_pizza,$current_ingredient);

//Fin de la commande
$current_commande->recapPanier();
unset($current_commande);



echo "\n\n\n\n\n";



echo "\nLE CLIENT B ARRIVE SUR LA BORNE\n\n";
$current_commande = new modele\Commande;

//Affichage de la carte
$piz->getCarte();

//Selection d'une pizza et affichage de sa recette
$current_pizza = $piz->getPizzaById(3);
echo "Vous avez sélectionné une pizza : ".strtoupper($current_pizza[0]["nom"])." - ".$current_pizza[0]["prix"]."€";
$current_ingredient = $ing->getIngredientByPizzaId($current_pizza[0]["id"]);
echo $current_ingredient;


//Ajout d'un ingrédient
$update = $ing->addIngredient($current_pizza, $current_ingredient, 11);
$current_pizza[0]["nom"] = $update["nom"];
$current_ingredient = $update["ingredient"];
echo "Vous avez modifié la pizza : ".strtoupper($current_pizza[0]["nom"])." - ".$current_pizza[0]["prix"]."€";
echo $current_ingredient;

//Supression d'un ingrédient
$update = $ing->supIngredient($current_pizza, $current_ingredient, 10);
$current_pizza[0]["nom"] = $update["nom"];
$current_ingredient = $update["ingredient"];
echo "Vous avez modifié la pizza : ".strtoupper($current_pizza[0]["nom"])." - ".$current_pizza[0]["prix"]."€";
echo $current_ingredient;

//Ajout de la pizza au panier
$current_commande->addPanier($current_pizza,$current_ingredient);

//Fin de la commande
$current_commande->recapPanier();
unset($current_commande);



echo "\n\n\n\n\n";



echo "\nLE CLIENT C ARRIVE SUR LA BORNE\n\n";
$current_commande = new modele\Commande;

//Affichage de la carte
$piz->getCarte();

//Selection d'une pizza et affichage de sa recette
$current_pizza = $piz->getPizzaById(0);
echo "Vous avez sélectionné une pizza : ".strtoupper($current_pizza[0]["nom"])." - ".$current_pizza[0]["prix"]."€";
$current_ingredient = $ing->getIngredientByPizzaId($current_pizza[0]["id"]);
echo $current_ingredient;


//Ajout d'un ingrédient
$update = $ing->addIngredient($current_pizza, $current_ingredient, 2);
$current_pizza[0]["nom"] = $update["nom"];
$current_ingredient = $update["ingredient"];
echo "Vous avez modifié la pizza : ".strtoupper($current_pizza[0]["nom"])." - ".$current_pizza[0]["prix"]."€";
echo $current_ingredient;

//Ajout d'un ingrédient
$update = $ing->addIngredient($current_pizza, $current_ingredient, 4);
$current_pizza[0]["nom"] = $update["nom"];
$current_ingredient = $update["ingredient"];
echo "Vous avez modifié la pizza : ".strtoupper($current_pizza[0]["nom"])." - ".$current_pizza[0]["prix"]."€";
echo $current_ingredient;

//Ajout d'un ingrédient
$update = $ing->addIngredient($current_pizza, $current_ingredient, 5);
$current_pizza[0]["nom"] = $update["nom"];
$current_ingredient = $update["ingredient"];
echo "Vous avez modifié la pizza : ".strtoupper($current_pizza[0]["nom"])." - ".$current_pizza[0]["prix"]."€";
echo $current_ingredient;

//Ajout d'un ingrédient
$update = $ing->addIngredient($current_pizza, $current_ingredient, 7);
$current_pizza[0]["nom"] = $update["nom"];
$current_ingredient = $update["ingredient"];
echo "Vous avez modifié la pizza : ".strtoupper($current_pizza[0]["nom"])." - ".$current_pizza[0]["prix"]."€";
echo $current_ingredient;

//Ajout de la pizza au panier
$current_commande->addPanier($current_pizza,$current_ingredient);

//Fin de la commande
$current_commande->recapPanier();
unset($current_commande);

     
?>

