<?php

namespace modele;

class ClassLoader extends AbstractClassLoader{
    
    protected $prefix = '';
    public $resultat = "";

    public function __construct($file_root)
    {
        $this->prefix = $file_root;
    }
    
    public function loadClass(string $classname)
    {
        try
        {
            include_once $this->makePath($this->getFilename($classname));
        }
        catch (Exception $e)
        {
            return $e;
        }
    }

    protected function makePath(string $filename): string
    {
        return $this->prefix.DIRECTORY_SEPARATOR.$filename;
    }

    protected function getFilename(string $classname): string
    {
        return $path = str_replace("\\", DIRECTORY_SEPARATOR, $classname).".php";
    }

}

