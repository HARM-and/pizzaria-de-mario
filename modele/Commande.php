<?php
namespace modele;

use PDO;

class Commande
{
  private $BDD;
  public $id_commande;

  function __construct()
  {
    $this->BDD = new Data;

    $this->id_commande = $this->createNewCommande()["MAX(id)"];
  }

  //Créer une nouvelle instance de commande
  function createNewCommande()
  {
    $resultat = "";

      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("insert into `commande` (`id`, `time_command`) values (NULL, current_timestamp());");
          $req->execute();
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }

      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select MAX(id) from `commande`");
          $req->execute();
          $resultat = $req->fetch(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
  }

  //Ajouter une pizza au panier
  function addPanier($pizza_data, $ingredient_data)
  {
    $id_pizza = $pizza_data[0]["id"];
    try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("insert into `panier` (`id_commande`, `id_pizza`, `ingredient`) values (:id_com, :id_piz, :ingr)");
          $req->bindValue(":id_com", $this->id_commande, PDO::PARAM_STR);
          $req->bindValue(":id_piz", $id_pizza, PDO::PARAM_STR);
          $req->bindValue(":ingr", $ingredient_data, PDO::PARAM_STR);
          $req->execute();
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
  }

  //Faire un recapitulatif du panier (prix total)
  function recapPanier()
  {
    $resultat = "";

      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select SUM(prix) FROM `pizza` WHERE id IN (SELECT id_pizza from `panier` where id_commande = :id_com)");
          $req->bindValue(":id_com", $this->id_commande, PDO::PARAM_STR);
          $req->execute();

          $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
          $total_price = $resultat[0]["SUM(prix)"];
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }

        echo "Votre panier vous reviens à ".$total_price."€";
  }

    
}

?>