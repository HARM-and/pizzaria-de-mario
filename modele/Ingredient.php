<?php
namespace modele;

use PDO;

class Ingredient
{
  private $BDD;

  function __construct()
  {
    $this->BDD = new Data;
  }
    //Obtenir les ingredients d'une pizza
    function getIngredientByPizzaId($id)
    {
      $resultat = array();

      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select ingredient.nom FROM pizza, recette, ingredient where pizza.id = recette.id_pizza and recette.id_ingredient = ingredient.id and pizza.id = :id");
          $req->bindValue(":id", $id, PDO::PARAM_STR);
          $req->execute();

          $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }

        $liste_ingredient = "";
        for ($i=0; $i < count($resultat) ; $i++)
        { 
          $liste_ingredient .="\n -".$resultat[$i]["nom"];
        }
        return $liste_ingredient."\n\n";
    }

    //Ajouter un ingrédient
    function addIngredient($pizza_data, $current_ingredient, $new_ingredient)
    {
      $resultat = array();
      
      if(substr($pizza_data[0]["nom"], -3)!="[C]")
      {
        $resultat["nom"] = $pizza_data[0]["nom"]." [C]";
      }
      else
      {
        $resultat["nom"] = $pizza_data[0]["nom"];
      }
      $resultat["ingredient"] = rtrim($current_ingredient, "\n")."\n -".$this->getIngredientById($new_ingredient)[0]["nom"]."\n\n";

      return $resultat;
    }

    //Enlever un ingredient
    function supIngredient($pizza_data, $current_ingredient, $ingredient)
    {
      $resultat = array();
      
      if(substr($pizza_data[0]["nom"], -3)!="[C]")
      {
        $resultat["nom"] = $pizza_data[0]["nom"]." [C]";
      }
      else
      {
        $resultat["nom"] = $pizza_data[0]["nom"];
      }
      $resultat["ingredient"] = str_replace(("\n -".$this->getIngredientById($ingredient)[0]["nom"]), "", $current_ingredient);

      return $resultat;
    }

    //Obtenir les infos d'un ingrédient 
    function getIngredientById($id)
    {
      $resultat = array();

      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from Ingredient where id=:id");
          $req->bindValue(":id", $id, PDO::PARAM_STR);
          $req->execute();

          $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
    }
}

?>