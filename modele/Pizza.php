<?php
namespace modele;

use PDO;

class Pizza
{
  private $BDD;

  function __construct()
  {
    $this->BDD = new Data;
  }
    //Obtenir toutes les infos sur toutes les pizzas existente
    function getAllPizza()
    {
      $resultat = array();

      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from pizza");
 
          $req->execute();

          $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
    }

    // Obtenir la liste des pizzas, leurs ingrédients ainsi que leur prix sous forme de menu
    function getCarte()
    {
      $ing = new Ingredient;
      $allPizza = $this->getAllPizza();
      $carte = "<----------------------------CARTE DES PIZZAS------------------------->\n";
      foreach ($allPizza as $Pizza)
      {
        $recipe = 

        $carte .= "".$Pizza["id"]." --- ".strtoupper($Pizza["nom"])." (".$Pizza["prix"]."€) : ";
        
        $carte .= $ing->getIngredientByPizzaId($Pizza["id"]);

      }
      echo $carte."<--------------------------------------------------------------------->\n";
    }

    //Obtenir les infos d'une pizza 
    function getPizzaById($id)
    {
      $resultat = array();

      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from Pizza where id=:id");
          $req->bindValue(":id", $id, PDO::PARAM_STR);
          $req->execute();

          $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
    }

}

?>